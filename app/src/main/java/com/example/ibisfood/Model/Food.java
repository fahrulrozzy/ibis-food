package com.example.ibisfood.Model;

public class Food {

    private String Description, Discount, Image, MenuId, FoodName, Price;

    public Food() {
    }

    public Food(String description, String discount, String image, String menuId, String foodName, String price) {
        Description = description;
        Discount = discount;
        Image = image;
        MenuId = menuId;
        FoodName = foodName;
        Price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getMenuId() {
        return MenuId;
    }

    public void setMenuId(String menuId) {
        MenuId = menuId;
    }

    public String getFoodName() {
        return FoodName;
    }

    public void setFoodName(String foodName) {
        FoodName = foodName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }
}
