package com.example.ibisfood;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.ibisfood.AdminUI.AdminActivity;
import com.example.ibisfood.UserUI.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignInActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private EditText username, password;
    private Button btnLogin;
    Switch active;
//    private Intent HomeActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        //ini
        username = findViewById(R.id.edtNoKamar);
        password = findViewById(R.id.edtPwNoKamar);
        btnLogin = findViewById(R.id.btnLoginUser);
        active = findViewById(R.id.active);
//        HomeActivity = new Intent(this, com.example.ibisfood.UserUI.MainActivity.class);


        auth = FirebaseAuth.getInstance();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                databaseReference.child("login").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String input1 = username.getText().toString();
                        String input2 = password.getText().toString();

                        if (dataSnapshot.child(input1).exists()){
                            if(dataSnapshot.child(input1).child("password").getValue(String.class).equals(input2)){
                                if (active.isChecked()){

                                    if (dataSnapshot.child(input1).child("as").getValue(String.class).equals("admin")){
                                        preferences.setDataLogin(SignInActivity.this, true);
                                        preferences.setDataAs(SignInActivity.this,"admin");
                                        startActivity(new Intent(SignInActivity.this, AdminActivity.class));

                                    } else if (dataSnapshot.child(input1).child("as").getValue(String.class).equals("user")) {

                                        preferences.setDataLogin(SignInActivity.this, true);
                                        preferences.setDataAs(SignInActivity.this,"user");
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    }

                                } else {

                                    if (dataSnapshot.child(input1).child("as").getValue(String.class).equals("admin")){
                                        preferences.setDataLogin(SignInActivity.this, false);
                                        startActivity(new Intent(SignInActivity.this, AdminActivity.class));

                                    } else if (dataSnapshot.child(input1).child("as").getValue(String.class).equals("user")) {

                                        preferences.setDataLogin(SignInActivity.this, false);
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    }

                                }

                            } else {
                                Toast.makeText(SignInActivity.this,"Kata Sandi Salah",Toast.LENGTH_SHORT).show();
                            }


                        } else {
                            Toast.makeText(SignInActivity.this,"Data belum terdaftar",Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });


//
//        btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String EmailKamar = inputEmailKamar.getText().toString().trim();
//                String PasswordKamar = inputPwKamar.getText().toString().trim();
//
//                if (EmailKamar.isEmpty() && PasswordKamar.isEmpty()) {
//                    Toast.makeText(getApplicationContext(), "Masukkan data dengan lengkap", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//
//
//                auth.signInWithEmailAndPassword(EmailKamar, PasswordKamar).addOnCompleteListener(SignInActivity.this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (!task.isSuccessful()) {
//                            Toast.makeText(getApplicationContext(), "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
//                            startActivity(intent);
//                            finish();
//                        }
//                    }
//                });
//
//
//            }
//        });

//
//        pindahLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(SignInActivity.this, SignInKitchenActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });



    }

    @Override
    protected void onStart () {
        super.onStart();

        if(preferences.getDataLogin(this)){
            if(preferences.getDataAs(this).equals("admin")){
                startActivity(new Intent(SignInActivity.this, AdminActivity.class));
                finish();
            } else if (preferences.getDataAs(this).equals("user")) {
                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                finish();
            }
        }
//
//        FirebaseUser user = auth.getCurrentUser();
//
//        if (user != null) {
//            //ketika pengisian sudah maka dipindahkan ke home activity
//            updateUI();
//        }
    }

//    private void updateUI() {
//        startActivity(HomeActivity);
//        finish();
//    }
}

