package com.example.ibisfood.UserUI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ibisfood.Interface.ItemClickListener;
import com.example.ibisfood.Model.Food;
import com.example.ibisfood.R;
import com.example.ibisfood.ViewHolder.FoodViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

public class Food_List_UserActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference foodList;

    String categoriId="";

    FirebaseRecyclerAdapter<Food, FoodViewHolder> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food__list__user);


        //Firebase
        database = FirebaseDatabase.getInstance();
        foodList = database.getReference().child("Food");

        recyclerView = (RecyclerView)findViewById(R.id.rv_food_list);
//        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //get Intent here
        if (getIntent() != null)
            categoriId = getIntent().getStringExtra("CategoriId");
        if (!categoriId.isEmpty() && categoriId != null){
            loadListFood(categoriId);
        }
    }

    private void loadListFood(final String categoriId) {

        Query query = FirebaseDatabase.getInstance().getReference().child("Food");

        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<Food>().setQuery(query,Food.class).build();

        adapter = new FirebaseRecyclerAdapter<Food, FoodViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FoodViewHolder holder, int position, @NonNull Food model) {

                holder.txtFoodName.setText(model.getFoodName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(holder.imageFood);

                final Food local = model;
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Toast.makeText(Food_List_UserActivity.this,""+local.getFoodName(),Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item,parent,false);
                FoodViewHolder viewHolder = new FoodViewHolder(view);
                return viewHolder;
            }
        };
        Log.d("TAG",""+adapter.getItemCount());
        recyclerView.setAdapter(adapter);
    }


}