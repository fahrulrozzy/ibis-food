package com.example.ibisfood.UserUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.ibisfood.Interface.ItemClickListener;
import com.example.ibisfood.Model.Category;
import com.example.ibisfood.R;
import com.example.ibisfood.ViewHolder.MenuViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MenuFragment extends Fragment {
    private FloatingActionButton fab;

    FirebaseDatabase mDatabase;
    DatabaseReference mCategory;

     FirebaseRecyclerAdapter<Category,MenuViewHolder> adapter;


    RecyclerView recycler_Menu;
    RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

//        mauth = FirebaseAuth.getInstance();
//        currentUserID = mauth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance();
        mCategory = mDatabase.getInstance().getReference().child("Categori");



        //load gambar
        recycler_Menu = (RecyclerView) view.findViewById(R.id.rv_food_category);
        layoutManager = new LinearLayoutManager(getContext());
        recycler_Menu.setLayoutManager(layoutManager);





        loadMenu();






        fab = (FloatingActionButton) view.findViewById(R.id.fab_shop_cart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Pesan Dahareun Buru!!",Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(getContext(),Food_List_UserActivity.class);
//                startActivity(intent);
            }
        });
        return view;
    }


    public void loadMenu() {

        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Categori");


        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<Category>().setQuery(query, Category.class).build();

        adapter = new FirebaseRecyclerAdapter<Category, MenuViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final MenuViewHolder holder, int position, @NonNull Category model) {

                holder.txtMenuNameCategory.setText(model.getName());
                Picasso.with(getContext()).load(model.getImage()).into(holder.imageMenuCategory);


                final Category clickItem = model;
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //klik ke food list activity
                        Intent foodList = new Intent(getContext(),Food_List_UserActivity.class);
                        //Because CategoriId is Key, so we just get key of this item
                        foodList.putExtra("CategoriId",adapter.getRef(position).getKey());
                        startActivity(foodList);

                    }
                });

            }


            @NonNull
            @Override
            public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item, parent, false);
                MenuViewHolder viewHolder = new MenuViewHolder(view);
                return viewHolder;
            }

        };

        recycler_Menu.setAdapter(adapter);
        adapter.startListening();
    }



}
